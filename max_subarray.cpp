#include <iostream>
#include <cmath>
#include <tuple>

using namespace std;

tuple <int , int,int > crossing_point(int *a,int low, int mid,int high);
tuple <int, int,int> max_subarray(int *a, int low, int high);

int main(){
    int arr[10]={-1,2,0,-2,-1,2,8,0,-8,9};
    int low, high, sum;
    tie( low, high,  sum)=max_subarray(arr,0,9);
    cout<<"low:"<<low << "high:" <<high << "sum:" << sum;

}

tuple <int, int,int> max_subarray(int *a, int low, int high){
    int new_arr[3];
    int left_low,left_high,left_sum,right_low,right_high,right_sum;
    int cross_low,cross_high,cross_sum;
    if (low==high){
    return {low,high,a[low]};
    }
    else{
        int mid=floor((low+high)/2);
        tie(left_low,left_high,left_sum)=max_subarray(a,  low,  mid);
        tie(right_low,right_high,right_sum)=max_subarray(a,mid+1,high);
        tie(cross_low,cross_high,cross_sum)=crossing_point(a, low,mid,high);
        if (left_sum>=right_sum & left_sum>=cross_sum){
            return {left_low,left_high,left_sum};
        }
        else if (right_sum>=left_sum & right_sum>=cross_sum){
            return {right_low,right_high,right_sum};
        }
        else{
            return {cross_low,cross_high,cross_sum};
        }
    }
}

tuple<int,int,int> crossing_point(int *a,int low, int mid,int high){
    int left_sum=-10000;
    int left=0;
    int left_index;
    for(int i=mid;i>=low;i--){
        left+=a[i];
        if (left > left_sum){
            left_sum=left;
            left_index=i;
        }

    }
    int right_sum=-100000;
    int right=0;
    int right_index;
    for (int i=mid+1;i<=high;i++){
        right+=a[i];
        if (right>right_sum){
            right_sum=right;
            right_index=i;
        }
    }
    return {left_index,right_index, right_sum};
}
#include<iostream>
#include<cmath>
#include<tuple>
using namespace std;
tuple<int, int> give_multiple();
int main(){
    int i,j;
    tie(i,j)=give_multiple();
    cout << i << endl;
}
tuple<int, int> give_multiple(){
    return make_tuple(20, 10);
}
// int main(){
//     int ar1[2][2]={{1,2},{3,4}};
//     int ar2[2][2]={{1,2},{3,4}};
//     int rows =  sizeof(ar1)/ sizeof(ar1[0]); // 2 rows  

//     int cols = sizeof (ar1[0]) / sizeof(int); // 5 cols
//     for (int row=0;row<rows;row++){
//         for(int col=0;col<cols;col++){
//             int val=0;
//             for(int k=0;k<cols;k++){
//                 val+=ar1[row][k]*ar2[k][col];
//             }
//             cout<< val << endl;

//         }
//     }
    
//     }
