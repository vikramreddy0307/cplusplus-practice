// Let A : : n be an array of n distinct numbers. If i<j and A[i] > A[j] , then the
// pair .i; j / is called an inversion of A.
// a. List the five inversions of the array h2; 3; 8; 6; 1i.

#include <iostream>
#include <cmath>
#include <vector>
#include <list>

using namespace std;
void inversion(int *a,int size);

int main(){
    int arr[5]={2,3,8,6,1};

    int sum=7;
    int size=sizeof(arr)/sizeof(arr[0]);
    inversion(arr,size);
}

void inversion(int *a,int size){
    // list< pair<int, float> > lister;

    vector< pair <int , int> > vect;


    for (int i=0;i<size;i++){
        for (int j=i+1;j<size;j++){
            if (a[i]>a[j]){
                vect.push_back(make_pair(i,j)); 
                }
        }
    }
    for (int i=0;i<size;i++){
        cout << vect[i].first << " " << vect[i].second << endl;
    }

}