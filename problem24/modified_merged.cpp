#include <iostream>
#include <cmath>
using namespace std;

int merge(int *a,int first,int mid,int end,int inversion);
int merge_sort(int *a, int start, int last,int inversion);

int main(){
    int arr[5]={2,3,8,6,1};
    int size=sizeof(arr)/sizeof(arr[0]);
    int inversion=0;
    inversion=merge_sort(arr,0,size-1,inversion);
    for(int i=0;i<size;i++){
        cout << arr[i] << endl;

    }
    cout << "inversion:"<< inversion;

    
}
int merge_sort(int *a,int start,int end,int inversion){
    if (start<end){
        int mid=floor((start+end)/2);
        inversion=merge_sort(a,start,mid,inversion);
        inversion=merge_sort(a,mid+1,end,inversion);
        inversion=merge(a,start, mid, end,inversion);
    }
    return inversion;
}
int merge(int *a, int start, int mid, int end,int inversion){
    int i=start;
    int j=mid+1;
    int k=0;
    int merged[end-start+1];
    while(i<=mid && j<=end){
        if (a[i]>a[j]){
            merged[k]=a[j];
            // cout<<"inv" << i << "-" <<a[i]<<"-" << j << "-" <<a[j]<< endl;
            
            j++;
            k++;
            inversion=inversion+1;
            // cout << "inversion:"<< inversion<< endl;
        }
        else{
            merged[k]=a[i];
            // inversion++;
            i++;
            k++;
        }

    }
    while(i<=mid){
        merged[k]=a[i];
        i++;
        k++;
        // inversion++;
    }
    while(j<=end){
        merged[k]=a[j];
        j++;
        k++;
        // cout<<"inv" << i << "-" <<a[i]<<"-" << j << "-" <<a[j]<< endl;

        inversion=inversion+1;
    }
    // for(int i=0;i<end-start+1;i++){
    //     cout<< merged[i]<< endl;
    // }
    // k=0;
    // for(int i=start;i<=end;i++){
    //     a[i]=merged[k];
    //     k++;
    //     }

return inversion;
}