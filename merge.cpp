#include <iostream>
#include <cmath>
using namespace std;

void merge(int *a,int low, int mid, int high);
void merge_sort(int *a,int start, int end );
int main(){
    int a[8]={4,5,3,1,2,99,-1,7};
    int size=sizeof(a)/sizeof(a[0]);
    cout << "size-" <<size << endl;
    merge_sort(a,0,size-1);
    for (int i=0;i<8;i++){
        cout << a[i] << endl;
    }
}
 void merge_sort(int *a,int start, int end ){
     if (start < end){

         int mid= floor((start+end)/2);
         cout << "mid:" << mid << endl;
         merge_sort(a,start,mid);
         merge_sort(a,mid+1,end);
         merge(a,start,mid,end);
     }
 }
 void merge( int *a, int low,int mid, int end){
    int merged[end-low+1];

    // Merging and sorting
    int l=low;
    int j=mid;
    int  k=0;
    while(l<=mid && j+1<=end){
        if (a[l]<=a[j+1]){
            merged[k]=a[l];
            k++;
            l++;
        }
        else{

            merged[k]=a[j+1];
            k++;
            j++;
        }

        
    }
    while (l<=mid){
        merged[k]=a[l];
        l++;
        k++;

    }
   while (j+1<=end){
        merged[k]=a[j+1];
        j++;
        k++;
        
    }
    // restoring back to original data
    k=0;
    for (int i=low;i<=end;i++){
        a[i]=merged[k];
        k++;
    }
 }
    