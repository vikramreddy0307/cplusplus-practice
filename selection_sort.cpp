#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

void selection_sort(vector<int> vect);

int main(){
    int arr[] = {1,6,0,9,100};

    vector<int> a (arr, arr + sizeof(arr) / sizeof(arr[0]));
    selection_sort(a);
}
void selection_sort(vector<int> vect){
    for (int i=0;i<vect.size()-1;i++){
        int min=i;
        for (int j=i+1;j<vect.size();j++){
            if (vect[j]<vect[i]){
                min=j;
            }
        }
        // Swapping min index element and i(th)  element
        int swap=vect[i];
        vect[i]=vect[min];
        vect[min]=swap;


    }
    for (int i=0;i<vect.size();i++){
        cout << vect[i] << endl;
    }
}