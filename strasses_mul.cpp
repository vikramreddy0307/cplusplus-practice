#include <iostream>
#include<cmath>
#include <tuple>
using namespace std;
int** strassen_mult(int **a, int **b,int size);
int** split_matrix(int **a,int row,int col,int size);
int ** add_matrices(int** a, int **b, int** c,int size);
int ** combine_matrices(int** a, int **b, int** c,int **d,int size);
int** get_array();
int main(){
    int **a,**b;
    
    // int b[4][4]={{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}};
    a=get_array();
    b=get_array();

    strassen_mult(a, b,4);
}
int ** get_array(){
    // int a;
    int A[4][4]={{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}};
    // for(int n=0;n<4;n++){
    //     for (int i=0;i<4;i++){
    //         a[n][i]=A[n][i];
    //         }
    //     }
        return A;
}
int** split_matrix(int **a, int row,int col,int size){
    int  **sub;
    for(int n=row;n<row+size/2;n++){
        for (int i=col;i<col+size/2;i++){
            sub[n][i]=a[n+row][col+i];
            }
        }
    return sub;
    }
int** add_matrices(int**a,int**b, int**c,int size){

    for(int row=0;row<size/2;row++){
        for (int col=0;col<size/2;col++){
            c[row][col]=a[row][col]+b[row][col];
        }

    }
    return c;
    }
int** combine_matrices(int** a, int **b, int** c,int **d,int size){
    int **Combo;
    for(int row=0;row<size/2;row++){
        for (int col=0;col<size/2;col++){
            Combo[row][col]=a[row][col];
        }

    }
     for(int row=0;row<size/2;row++){
        for (int col=0;col<size/2;col++){
            Combo[row][col+size/2]=b[row][col];
        }

    }
     for(int row=0;row<size/2;row++){
        for (int col=0;col<size/2;col++){
            Combo[row+size/2][col]=c[row][col];
        }

    }
     for(int row=0;row<size/2;row++){
        for (int col=0;col<size/2;col++){
            Combo[row+size/2][col+size/2]=d[row][col];
        }

    }
    return Combo;
}
int** strassen_mul(int **a, int **b,int size){
    int rows =  size/ sizeof(a[0][0]);  
    int** C;
    if (rows==1){
        for(int i=0;i<1;i++){
            for (int j=0;j<1;j++){
                C[i][j]=a[j][j]*b[j][j];
            }
        }
        ;
    }
    else{
        int **A11,**A12,**A21,**A22,**B11,**B12,**B21,**B22;
        // Divide the array in to four subarrays of n/2 * n/2
        A11=split_matrix(a, 0,0,size);
        A12=split_matrix(a, 0,size/2,size);
        A21=split_matrix(a, size/2,0,size);
        A22=split_matrix(a, size/2,size/2,size);
        B11=split_matrix(b, 0,0,size);
        B12=split_matrix(b, 0,size/2,size);
        B21=split_matrix(b, size/2,0,size);
        B22=split_matrix(b, size/2,size/2,size);
        int **m1,**m2,**C11,**C12,**C21,**C22;
      
        m1=strassen_mul(A11,B11, size);
        m2=strassen_mul(A12,B21, size);
        C11=add_matrices(m1,m2,C11,size);

        m1=strassen_mul(A11,B12, size);
        m2=strassen_mul(A12,B22, size);
        C12=add_matrices(m1,m2,C12,size);

        m1=strassen_mul(A21,B11, size);
        m2=strassen_mul(A22,B21, size);
        C21=add_matrices(m1,m2,C21,size);

        m1=strassen_mul(A21,B12, size);
        m2=strassen_mul(A22,B22, size);
        C22=add_matrices(m1,m2,C22,size);

        // Combine matrices
        
        C=combine_matrices(C11,C12,C21,C22,size);



    }
    return C;
    }