#include<iostream>
#include <cmath>
using namespace std;

void merge(int *a,int start, int mid,int end);
void merge_sort(int *a, int start, int end);
int problem_237(int *a,int sum);

int main(){
     int a[10]={1,2,3,4,10,9,8,7,6,5};
     int size=sizeof(a)/sizeof(a[0]);
     merge_sort(a,0,size-1);
    //  for (int i=0;i<size;i++){
    //      cout << a[i] << endl;
    //  }
     int sum=10;
     bool val=problem_237(a, sum);
     cout << val << endl;
 }
//  I am getting size as 2;
int problem_237(int *a, int sum){
    int size1=sizeof(a)/sizeof(a[0]);
    cout << "size " << size1<< endl;
    int i=0;
    while(i<size1){
        cout<< i << size1 << endl;
        if (a[i]+a[size1]==sum){
            return 1;
        }
        else if (a[i]+a[size1]< sum){
            i++;
        }
        else {
            size1--;
        }
    }
    return 0;


}
void merge_sort(int *a,int start, int end){
    if (start<end){
        
        int mid= floor((start+end)/2);
        // cout << "mid" << mid << endl;
        // cout << mid << endl;
        merge_sort(a,start,mid);
        merge_sort(a,mid+1,end);
        merge(a,start,mid,end);
    }
}

void merge(int *a, int start, int mid, int end){
    
    // cout << "Before" << endl;
    // for(int i=start;i<=end;i++){
    //     cout<< a[i] << endl;
    // }
    int i=start;
    int j=mid,k=0;
    int c[end-start+1];
    while(i<=mid && j+1<=end){
        if (a[i]<=a[j+1]){
            c[k]=a[i];
            k++;
            i++;

        }
        else{
            c[k]=a[j+1];
            k++;
            j++;

        }
    }
    while(i<=mid){
        c[k]=a[i];
        k++;
        i++;
    }
    while(j+1<=end){
        c[k]=a[j+1];
        j++;
        k++;
    }
    // cout << "After" << endl;
    // for (i=0;i<end-start+1;i++){
    //     cout<< c[i] << endl;
    // }
    // Copying back to original array
    k=start;
    for(i=0;i<end-start+1;i++){
        // cout << c[i] << endl;
        a[k]=c[i];
        k++;


    }

}